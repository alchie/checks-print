<?php $this->load->view('header'); ?>

<body class="nav-md">

  <div class="container body">


    <div class="main_container">

	   <?php $this->load->view('top_nav'); ?>

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="clearfix"></div>

          <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
					<a class="pull-right btn btn-danger btn-xs" href="<?php echo site_url("payee/delete/" . $payee->id); ?>"><i class="fa fa-trash"></i> Delete Payee</a>
					<a class="pull-right btn btn-success btn-xs" href="<?php echo site_url("payee/edit/" . $payee->id); ?>"><i class="fa fa-pencil"></i> Edit Payee</a>
                  <h2><?php echo $payee->payee; ?></h2>
                  <div class="clearfix"></div>

 <div class="x_content">
				  <?php 
				  if( validation_errors() ) {
					echo "<div class=\"alert alert-danger\">";
					echo validation_errors(); 
					echo "</div>";
				  }
				  ?>
                  <br />
                  <?php echo form_open("payee/preview/" . $payee->id, array("id"=>"","class"=>"form-horizontal form-label-left","method"=>'get', "target"=>"_blank")); ?>
<?php
	
	$signas = array();
	foreach( $signatories as $signatory ) {
		$signas[$signatory->id] = $signatory->signatory;
	}
	$forms = array(
		'check_num' => array("title"=>"Check Number", 'type'=>"text", "attributes"=>array("required"=>"required"), "default"=>""),
		'amount' => array("title"=>"Amount", 'type'=>"text", "attributes"=>array("required"=>"required"), "default"=>""),
		'date' => array("title"=>"Date", 'type'=>"datepicker", "attributes"=>array("required"=>"required"), "default"=>date("m/d/Y")),
		'signatory' => array("title"=>"Signatory", 'type'=>"checkbox2", "attributes"=>array("required"=>"required"), 'default'=>'', "options"=>$signas),
		);
	
	foreach($forms as $key=>$form ) {
		echo gentelella_form1( $form['type'], $form['title'], $key, $form, $form['default'] ); 
	}
	?>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success"><i class="fa fa-plus"></i> Preview</button>
						<a href="<?php echo site_url("payee"); ?>" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                      </div>
                    </div>

                  </form>				  
                </div>
				
<div class="x_panel">
                <div class="x_title">
                  <h2>Checks Issued</h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <table class="table">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Check Number</th>
                        <th>Date Issued</th>
                        <th>Amount</th>
            <th class="text-right">Preview</th>
                      </tr>
                    </thead>
                    <tbody>
					<?php foreach($checks_made as $check) { ?>
                      <tr>
                        <th scope="row"><?php echo $check->id; ?></th>
                        <td><?php echo $check->check_num; ?></td>
                        <td><?php echo $check->date; ?></td>
                        <td><?php echo number_format( $check->amount, 2 ); ?></td>
            <td class="text-right"><a href="<?php echo site_url("checks/preview/" . $check->id); ?>" class="btn btn-info btn-xs">Preview</a></td>
                      </tr>
					  <?php } ?>
                    </tbody>
                  </table>

                </div>
              </div>
			  
              </div>
            </div>
          </div>
        </div>

<?php $this->load->view('footer'); ?>
