<?php $this->load->view('header'); ?>

<body class="nav-md">

  <div class="container body">


    <div class="main_container">

	   <?php $this->load->view('top_nav'); ?>

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="clearfix"></div>

          <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Settings</h2>
                  <div class="clearfix"></div>

 <div class="x_content">
				  <?php 
				  if( validation_errors() ) {
					echo "<div class=\"alert alert-danger\">";
					echo validation_errors(); 
					echo "</div>";
				  }
				  ?>
                  <br />
                  <?php echo form_open(uri_string(), array("id"=>"","class"=>"form-horizontal form-label-left")); ?>
<?php
	$font_styles = array(
		'serif' => 'Serif',
		'sans-serif' => 'Sans-serif',
		'monospace' => 'Monospace',
		'cursive' => "Cursive",
		'fantasy' => "Fantasy"
	);
	$forms = array(
		'check_font_style' => array("title"=>"Check Font Style", 'type'=>"select_single", "attributes"=>array("required"=>"required"), "default"=>"serif", "options"=>$font_styles),
		);
	
	foreach($forms as $key=>$form ) {
		$default = (@$svs[$key]) ? $svs[$key] : $form['default'];
		echo gentelella_form1( $form['type'], $form['title'], 'settings['. $key .']', $form, $default ); 
	}
	?>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save Settings</button>
						<a href="<?php echo site_url("payee"); ?>" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                      </div>
                    </div>

                  </form>				  
                </div>
              </div>
            </div>
          </div>
        </div>

<?php $this->load->view('footer'); ?>
