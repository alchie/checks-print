<?php $this->load->view('header'); ?>

<body class="nav-md">

  <div class="container body">


    <div class="main_container">

	   <?php $this->load->view('top_nav'); ?>

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="clearfix"></div>

          <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2><?php echo $payee->payee; ?></h2>
                  <div class="clearfix"></div>

 <div class="x_content">
				  <?php 
				  if( validation_errors() ) {
					echo "<div class=\"alert alert-danger\">";
					echo validation_errors(); 
					echo "</div>";
				  } 
				  ?>
                  <br />
                  <?php echo form_open("", array("id"=>"","class"=>"form-horizontal form-label-left","method"=>'post')); ?>
<?php
	
	$signas = array();
	foreach( $signatories as $signatory ) {
		$signas[$signatory->id] = $signatory->signatory;
	}
	$forms = array(
		'check_num' => array("title"=>"Check Number", 'type'=>"text", "attributes"=>array("required"=>"required"), "default"=>$check_data->check_num),
		'amount' => array("title"=>"Amount", 'type'=>"text", "attributes"=>array("required"=>"required"), "default"=>$check_data->amount),
		'date' => array("title"=>"Date", 'type'=>"datepicker", "attributes"=>array("required"=>"required"), "default"=>date("m/d/Y", strtotime($check_data->date))),
		'signatory' => array("title"=>"Signatory", 'type'=>"checkbox2", "attributes"=>array("required"=>"required"), 'default'=>json_decode($check_data->signatory), "options"=>$signas),
		);
	
	foreach($forms as $key=>$form ) {
		echo gentelella_form1( $form['type'], $form['title'], $key, $form, $form['default'] ); 
	}
	?>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success"><i class="fa fa-plus"></i> Save and Preview</button>
						<a href="<?php echo site_url("checks/preview/{$check_data->id}"); ?>" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                      </div>
                    </div>

                  </form>				  
                </div>
				
			  
              </div>
            </div>
          </div>
        </div>

<?php $this->load->view('footer'); ?>
