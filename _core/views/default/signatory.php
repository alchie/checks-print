<?php $this->load->view('header'); ?>

<body class="nav-md">

  <div class="container body">


    <div class="main_container">

	   <?php $this->load->view('top_nav'); ?>

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          
          <div class="clearfix"></div>

          <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel" style="height:600px;">
                <div class="x_title">
                  <h2>Signatory List</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="btn btn-xs btn-success" href="<?php echo site_url("signatories/add"); ?>"><i class="fa fa-plus"></i> Add Signatory</a></li>
					<li><a class="btn btn-xs btn-danger" href="<?php echo site_url("payee"); ?>">Back to Payee</a></li>
                  </ul>
                  <div class="clearfix"></div>
				  <div class="x_content">
				  <div class="list-group">
					<?php foreach($list as $item) { ?>
						<a class="list-group-item" href="<?php echo site_url("signatories/edit/" . $item->id ); ?>"><?php echo $item->signatory; ?></a>
					<?php } ?>
				  </div>
				  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

<?php $this->load->view('footer'); ?>
