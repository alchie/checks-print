<?php $this->load->view('header'); ?>

<body class="nav-md">

  <div class="container body">


    <div class="main_container">

	   <?php $this->load->view('top_nav'); ?>

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="clearfix"></div>

          <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel" style="height:600px;">
                <div class="x_title">
                  <h2>Are you sure you want to delete Payee: <strong><?php echo $payee->payee; ?></strong>?</h2>
                  <div class="clearfix"></div>

 <div class="x_content">
				  <?php 
				  if( validation_errors() ) {
					echo "<div class=\"alert alert-danger\">";
					echo validation_errors(); 
					echo "</div>";
				  }
				  ?>
                  <br />
                  <?php echo form_open( uri_string(), array("id"=>"","class"=>"form-horizontal form-label-left")); ?>
				  <input type="hidden" name="payee_id" value="<?php echo $payee->id; ?>">
<?php
/*
	$forms = array(
		'payee' => array("title"=>"Payee", 'type'=>"text", "attributes"=>array("required"=>"required"), 'default'=>$payee->payee),
		);
	
	foreach($forms as $key=>$form ) {
		echo gentelella_form1( $form['type'], $form['title'], $key, $form, $form['default'] ); 
	}
*/
	?>
                    
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> YES</button>
						<a href="<?php echo site_url("payee/checks/" . $payee->id); ?>" class="btn btn-danger"><i class="fa fa-times"></i> NO</a>
                      </div>
                    </div>

                  </form>				  
                </div>
              </div>
            </div>
          </div>
        </div>

<?php $this->load->view('footer'); ?>
