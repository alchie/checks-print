<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Checks Print by Trokis Philippines</title>

  <!-- Bootstrap core CSS -->

  <link href="<?php echo base_url("assets/css/bootstrap.min.css"); ?>" rel="stylesheet">

  <link href="<?php echo base_url("assets/fonts/css/font-awesome.min.css"); ?>" rel="stylesheet">
  <link href="<?php echo base_url("assets/css/animate.min.css"); ?>" rel="stylesheet">

  <!-- Custom styling plus plugins -->
  <link href="<?php echo base_url("assets/css/custom.css"); ?>" rel="stylesheet">
  <link href="<?php echo base_url("assets/css/icheck/flat/green.css"); ?>" rel="stylesheet">


  <script src="<?php echo base_url("assets/js/jquery.min.js"); ?>"></script>

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
<style type="text/css" media="print">
    @page 
    {
        size: auto;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
    }
</style>
</head>
<body class="print">
				  	<div id="check_print" class="<?php echo (@$svs['check_font_style']) ? $svs['check_font_style'] : ""; ?>">
						 <div class="elm payee <?php echo ($payee->two_lines) ? 'two_lines' : ''; ?>"><?php echo strtoupper($payee->payee); ?></div>
						 <div class="elm date"><?php echo date("F d, Y",  strtotime($check_data->date)); ?></div>
						 <div class="elm amount"><?php echo number_format( $check_data->amount , 2 ); ?></div>
						 <div class="elm amount-words"><?php echo strtoupper( number2word( $check_data->amount ) ); ?></div>
						 <?php $n=1; 
						 foreach( $signatories as $signatory) {
							echo "<div class=\"elm signatory signatory-".$n."\">" . strtoupper($signatory->signatory) . "</div>";
							$n++;
						} 
						?>
					</div>
<script>
<!--
	window.print();
-->
</script>         
</body>
</html>
