<?php $this->load->view('header'); ?>

<body class="nav-md">

  <div class="container body">


    <div class="main_container">

	   <?php $this->load->view('top_nav'); ?>

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="">
              <div class="col-md-12 col-sm-12 col-xs-12 form-group pull-right top_search">
				<form method="get" action="<?php echo site_url( uri_string() ); ?>">
                <div class="input-group">
                  <input name="payee" type="text" class="form-control" placeholder="Search for Payee..." value="<?php echo $this->input->get("payee"); ?>">
                  <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">Search</button>
                        </span>
                </div>
				</form>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>

          <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Payee List</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="btn btn-xs btn-success" href="<?php echo site_url("payee/add"); ?>"><i class="fa fa-plus"></i> Add Payee</a></li>
					<li><a class="btn btn-xs btn-warning" href="<?php echo site_url("signatories"); ?>">Signatories</a></li>
					<li><a class="btn btn-xs btn-primary" href="<?php echo site_url("settings"); ?>"><i class="fa fa-cog"></i> Settings</a></li>
                  </ul>
                  <div class="clearfix"></div>
				  <div class="x_content">
				  <div class="list-group">
					<?php foreach($list as $item) { ?>
						<a class="list-group-item" href="<?php echo site_url("payee/checks/" . $item->id ); ?>"><?php echo $item->payee; ?></a>
					<?php } ?>
					<?php if($this->input->get('payee')!='') { ?>
				
						<center style="margin-top:20px;"><a class="btn btn-xs btn-danger" href="<?php echo site_url("payee"); ?>">Clear Search</a></center>
					<?php } ?>
				  </div>
				  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

<?php $this->load->view('footer'); ?>
