<?php $this->load->view('header'); ?>

<body class="nav-md">

  <div class="container body">


    <div class="main_container">

	   <?php $this->load->view('top_nav'); ?>

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="clearfix"></div>

          <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="">
                  <div class="clearfix"></div>
<?php echo form_open("payee/print/" . $payee->id, array("id"=>"","class"=>"form-horizontal form-label-left")); ?>
				  <?php echo strtoupper($payee->payee); ?>
				  <?php echo date("F d, Y",  strtotime($this->input->get('date'))); ?>
				   <input type="hidden" name="date" value="<?php echo $this->input->get('date'); ?>">
				  <?php echo $this->input->get('amount'); ?>
				  <input type="hidden" name="amount" value="<?php echo $this->input->get('amount'); ?>">
				  <?php foreach( $signatories as $signatory) {
					echo strtoupper($signatory->signatory) . "&nbsp;&nbsp;&nbsp;";
					echo '<input type="hidden" name="signatory[]" value="'.$signatory->id.'">';
						}
					?>
					<?php echo strtoupper( number2word( $this->input->get('amount') ) . " pesos only" ); ?>
				<div class="x_content">
				  				  
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
						
                        <button type="submit" class="btn btn-success"><i class="fa fa-print"></i> Save and Print</button>
						
						<a href="<?php echo site_url("payee"); ?>" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                      </div>
                    </div>
			  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

<?php $this->load->view('footer'); ?>
