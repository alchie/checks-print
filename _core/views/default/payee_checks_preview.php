<?php $this->load->view('header'); ?>

<body class="nav-md">

  <div class="container body">


    <div class="main_container">

	   <?php $this->load->view('top_nav'); ?>

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="clearfix"></div>

          <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div>
                  <div class="clearfix"></div>
<?php echo form_open("payee/save/" . $payee->id, array("id"=>"","class"=>"form-horizontal form-label-left")); ?>
				  <input type="hidden" name="check_num" value="<?php echo $this->input->get('check_num'); ?>">
				   <input type="hidden" name="date" value="<?php echo $this->input->get('date'); ?>">
				  <input type="hidden" name="amount" value="<?php echo $this->input->get('amount'); ?>">
				  <?php foreach( $signatories as $signatory) {
					echo '<input type="hidden" name="signatory[]" value="'.$signatory->id.'">';
						}
					?>
					
				<div class="x_content">
				  	<div id="check_preview">
						 <div class="elm check_num"><?php echo $this->input->get('check_num'); ?></div>
						 <div class="elm payee"><?php echo strtoupper($payee->payee); ?></div>
						 <div class="elm date"><?php echo date("F d, Y",  strtotime($this->input->get('date'))); ?></div>
						 <div class="elm amount"><?php echo number_format( $this->input->get('amount') , 2 ); ?></div>
						 <div class="elm amount-words"><?php echo strtoupper( number2word( $this->input->get('amount') ) ); ?></div>
						 <?php $n=1; 
						 foreach( $signatories as $signatory) {
							echo "<div class=\"elm signatory signatory-".$n."\">" . strtoupper($signatory->signatory) . "</div>";
							$n++;
						} 
						?>
					</div>
                    <div class="ln_solid"></div>
					<center>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success"><i class="fa fa-print"></i> Save and Print</button>
						
						<a href="javascript:history.back(-1);" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Back</a>
                      </div>
                    </div>
					</center>
			  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

<?php $this->load->view('footer'); ?>
