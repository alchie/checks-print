<?php $this->load->view('header'); ?>

<body class="nav-md">

  <div class="container body">


    <div class="main_container">

	   <?php $this->load->view('top_nav'); ?>

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="clearfix"></div>

          <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div>
                <a href="<?php echo site_url("checks/edit/" . $check_data->id); ?>" class="btn btn-success pull-right btn-xs"><i class="fa fa-pencil"></i> Edit</a>
                  <div class="clearfix"></div>
					
				<div class="x_content">
				  	<div id="check_preview">
						 <div class="elm check_num"><?php echo $check_data->check_num; ?></div>
						 <div class="elm payee <?php echo ($payee->two_lines) ? 'two_lines' : ''; ?>"><?php echo strtoupper($payee->payee); ?></div>
						 <div class="elm date"><?php echo date("F d, Y",  strtotime($check_data->date)); ?></div>
						 <div class="elm amount"><?php echo number_format( $check_data->amount , 2 ); ?></div>
						 <div class="elm amount-words"><?php echo strtoupper( number2word( $check_data->amount ) ); ?></div>
						 <?php $n=1; 
						 foreach( $signatories as $signatory) {
							echo "<div class=\"elm signatory signatory-".$n."\">" . strtoupper($signatory->signatory) . "</div>";
							$n++;
						} 
						?>
					</div>
                    
					<div class="ln_solid"></div>
					<center>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
						
                        
                        <a target="_blank" href="<?php echo site_url("checks/pr1nt/" . $check_data->id); ?>" class="btn btn-success"><i class="fa fa-print"></i> Print</a>
						<a href="<?php echo site_url("payee/checks/" . $check_data->payee); ?>" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                      </div>
                    </div>
</center>
                </div>
              </div>
            </div>
          </div>
        </div>

<?php $this->load->view('footer'); ?>
