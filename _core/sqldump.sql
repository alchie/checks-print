-- Table structure for table `checks` 

CREATE TABLE `checks` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `check_num` varchar(20) NOT NULL,
  `payee` int(10) NOT NULL,
  `date` varchar(50) NOT NULL,
  `amount` varchar(50) NOT NULL,
  `signatory` text NOT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `payee` 

CREATE TABLE `payee` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `payee` varchar(255) NOT NULL,
  `two_lines` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `payee` (`payee`)
);

-- Table structure for table `settings` 

CREATE TABLE `settings` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `key` varchar(100) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `signatory` 

CREATE TABLE `signatory` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `signatory` varchar(255) NOT NULL,
  `priority` int(5) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `signatory` (`signatory`)
);

