<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('number2word'))
{
	function number2word($number, $isFraction=false)
	{
		if( $isFraction ) {
			$number = ltrim($number, "0");
		}
		if( strpos( $number, "." ) > 0 ) {
			list($integer,$fraction) = explode(".", (string) $number);
		} else {
			$integer = $number;
		}
		$output = "";
		
		if( $integer{0} == "-") {
			$output = "negative ";
			$integer = ltrim($integer, "-");
		} 
		elseif( $integer{0} == "+") {
			$output = "positive ";
			$integer = ltrim($integer, "+");
		}
		if( $integer{0} == "0" ) {
			$output .= "zero ";
		} else {
			$integer = str_pad( $integer, 36, "0", STR_PAD_LEFT);
			$group = rtrim(chunk_split( $integer, 3, " "), " ");
			$groups = explode(" ", $group);
			$groups2 = array();
			foreach($groups as $g) {
				$groups2[] = _convertThreeDigits($g{0},$g{1},$g{2});
			}

			for($z=0;$z<count($groups2);$z++) {
				if( $groups2[$z] != "" ) {
					$output .= $groups2[$z] . _convertGroup(11-$z);
				}
			}
			$output = rtrim($output, ", ");
			
			if( ($isFraction==false) && isset($fraction) && ($fraction > 0) ) {
				$output .= " pesos & " . number2word($fraction, true) . " centavos only";
			} else {
				if ($isFraction==false) {
					$output .= " pesos only";
				}
			}
		}
		return $output;
	}
	
}

if ( ! function_exists('_convertGroup'))
{
	function _convertGroup( $index ) {
		switch( $index ) {
			case "11":
				return " decillion ";
			break;
			case "10":
				return " nonillion ";
			break;
			case "9":
				return " octillion ";
			break;
			case "8":
				return " septillion ";
			break;
			case "7":
				return " sextillion ";
			break;
			case "6":
				return " quintrillion ";
			break;
			case "5":
				return " quadrillion ";
			break;
			case "4":
				return " trillion ";
			break;
			case "3":
				return " billion ";
			break;
			case "2":
				return " million ";
			break;
			case "1":
				return " thousand ";
			break;
			case "0":
				return "";
			break;
		}
	}
}

if ( ! function_exists('_convertThreeDigits'))
{
	function _convertThreeDigits( $digit1, $digit2, $digit3 ) {
		$buffer = "";
		if( $digit1 == "0" && $digit2 == "0" && $digit3 == "0" ) {
			return "";
		}
		if( $digit1 != "0" ) {
			$buffer .= _convertDigit( $digit1 ) . " hundred";
			if( $digit2 != "0" || $digit3 != "0" ) {
				$buffer .= " "; //" and ";
			}
		}
		if( $digit2 != "0" ) {
			$buffer .= _convertTwoDigits( $digit2, $digit3 );
		} elseif( $digit3 != "0" ) {
			$buffer .= _convertDigit( $digit3 );
		}
		return $buffer;
	}
}

if ( ! function_exists('_convertTwoDigits'))
{
	function _convertTwoDigits( $digit1, $digit2 ) {
		if( $digit2 == "0" ) {
			switch( $digit1 ) {
				case "1":
					return "ten";
				break;
				case "2":
					return "twenty";
				break;
				case "3":
					return "thirty";
				break;
				case "4":
					return "forty";
				break;
				case "5":
					return "fifty";
				break;
				case "6":
					return "sixty";
				break;
				case "7":
					return "seventy";
				break;
				case "8":
					return "eighty";
				break;
				case "9":
					return "ninety";
				break;
			}
		} 
		elseif( $digit1 == "1") {
			switch( $digit2 ) {
				case "1":
					return "eleven";
				break;
				case "2":
					return "twelve";
				break;
				case "3":
					return "thirteen";
				break;
				case "4":
					return "fourteen";
				break;
				case "5":
					return "fifteen";
				break;
				case "6":
					return "sixteen";
				break;
				case "7":
					return "seventeen";
				break;
				case "8":
					return "eighteen";
				break;
				case "9":
					return "nineteen";
				break;
			}
		} 
		else {
			$temp = _convertDigit( $digit2 );
			switch( $digit1 ) {
				case "2":
					return "twenty-" . $temp;
				break;
				case "3":
					return "thirty-" . $temp;
				break;
				case "4":
					return "forty-" . $temp;
				break;
				case "5":
					return "fifty-" . $temp;
				break;
				case "6":
					return "sixty-" . $temp;
				break;
				case "7":
					return "seventy-" . $temp;
				break;
				case "8":
					return "eighty-" . $temp;
				break;
				case "9":
					return "ninety-" . $temp;
				break;
			}
		}
	}
}

if ( ! function_exists('_convertDigit'))
{
	function _convertDigit( $digit1 ) {
		switch( $digit1 ) {
			case "0":
				return "zero";
			break;
			case "1":
				return "one";
			break;
			case "2":
				return "two";
			break;
			case "3":
				return "three";
			break;
			case "4":
				return "four";
			break;
			case "5":
				return "five";
			break;
			case "6":
				return "six";
			break;
			case "7":
				return "seven";
			break;
			case "8":
				return "eight";
			break;
			case "9":
				return "nine";
			break;
		}
	}
}



// ------------------------------------------------------------------------

