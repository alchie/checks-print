<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signatories extends CI_Controller {

	var $data = array();
	
	public function __construct() {
		parent::__construct();
		$this->load->model('Signatory_model');
	}
	
	public function index()
	{
		$signa = new $this->Signatory_model;
		$signa->setOrder("signatory", "asc");
		$this->data['list'] = $signa->populate();
		$this->load->view('signatory', $this->data );
	}
	
	public function add() {
		if( count($this->input->post()) > 0) {
			$this->form_validation->set_rules('signatory', 'Signatory', 'trim|required|is_unique[signatory.signatory]');
			if( $this->form_validation->run() == TRUE ) {
				$signa = new $this->Signatory_model;
				$signa->setSignatory($this->input->post("signatory"));
				if( $signa->insert() ) {
					redirect("signatories", "location");
				}
			}
			
		}
		$this->load->view('signatory_add', $this->data );
	}
	
	public function edit($id) {
		$signa = new $this->Signatory_model;
		$signa->setId($id, true, false);
		if( count($this->input->post()) > 0) {
			$this->form_validation->set_rules('signatory', 'Signatory', 'trim|required');
			if( $this->form_validation->run() == TRUE ) {
				$signa->setSignatory($this->input->post("signatory"));
				$signa->setPriority($this->input->post("order"));
				if( $signa->update() ) {
					redirect("signatories", "location");
				}
			}
		}
		$this->data['signatory'] = $signa->get();
		$this->load->view('signatory_edit', $this->data );
	}
	
	public function delete($id) {
		$signa = new $this->Signatory_model;
		$signa->setId($id, true);
		if( count($this->input->post()) > 0) {
			$this->form_validation->set_rules('sid', 'Signatory ID', 'required');
			if( $this->form_validation->run() == TRUE ) {
				$signa->setId($this->input->post('sid'), true);
				if( $signa->delete() ) {
					redirect("signatories", "location");
				}
			}
		}
		$this->data['signatory'] = $signa->get();
		$this->load->view('signatory_delete', $this->data );
	}
}
