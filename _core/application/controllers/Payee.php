<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payee extends CI_Controller {

	var $data = array();
	
	public function __construct() {
		parent::__construct();
		$this->load->model('Payee_model');
	}
	
	public function index()
	{
		$payee = new $this->Payee_model;
		$payee->setOrder("payee", "asc");
		$payee->setLimit(0);
		
		if( $this->input->get('payee') != '') {
			$payee->setWhere("payee LIKE", "%".$this->input->get('payee')."%");
		}
		
		$this->data['list'] = $payee->populate();
		$this->load->view('payee', $this->data );
	}
	
	public function add() {
		if( count($this->input->post()) > 0) {
			$this->form_validation->set_rules('payee', 'Payee', 'trim|required|is_unique[payee.payee]');
			if( $this->form_validation->run() == TRUE ) {
				$payee = new $this->Payee_model;
				$payee->setPayee($this->input->post("payee"));
				$payee->setTwoLines(0);
				if( $payee->insert() ) {
					redirect("payee/checks/" . $payee->getId(), "location");
				}
			}
			
		}
		$this->load->view('payee_add', $this->data );
	}
	
	public function edit($id) {
		$payee = new $this->Payee_model;
		$payee->setId($id, true, false);
		if( count($this->input->post()) > 0) {
			$this->form_validation->set_rules('payee', 'Payee', 'trim|required');
			if( $this->form_validation->run() == TRUE ) {
				$payee->setPayee($this->input->post("payee"));
				$payee->setTwoLines(($this->input->post("two_lines")) ? 1 : 0);
				if( $payee->update() ) {
					redirect("payee/checks/" . $payee->getId(), "location");
				}
			}
		}
		$this->data['payee'] = $payee->get();
		$this->load->view('payee_edit', $this->data );
	}
	
	public function delete($id) {
		$payee = new $this->Payee_model;
		$payee->setId($id, true, false);
		if( count($this->input->post()) > 0) {
			$this->form_validation->set_rules('payee_id', 'Payee ID', 'trim|required');
			if( $this->form_validation->run() == TRUE ) {
				if( $this->input->post("payee_id") == $id) {
					if( $payee->delete() ) {
						redirect("payee", "location");
					}
				}
			}
		}
		$this->data['payee'] = $payee->get();
		$this->load->view('payee_delete', $this->data );
	}
	
	public function checks( $id ) {
		
		if( $this->input->get("print") != "" ) {
			redirect("checks/pr1nt/" . $this->input->get("print"), "location");
		} 
		$payee = new $this->Payee_model;
		$payee->setId($id, true);
		$this->data['payee'] = $payee->get();
		
		$this->load->model('Signatory_model');
		$signa = new $this->Signatory_model;
		$this->data['signatories'] = $signa->populate();
		
		$this->load->model('Checks_model');
		$checks = new $this->Checks_model;
		$checks->setPayee($id, true);
		$checks->setOrder('id', 'desc');
		$this->data['checks_made'] = $checks->populate();
		
		$this->load->view('payee_checks', $this->data );
	}
	
	public function preview( $id ) {
		
		$payee = new $this->Payee_model;
		$payee->setId($id, true);
		$this->data['payee'] = $payee->get();
		
		$this->load->model('Signatory_model');
		$signa = new $this->Signatory_model;
		$signa->setId($this->input->get('signatory'), true, false, NULL, 'where_in');
		$signa->setOrder('priority', 'ASC');
		$this->data['signatories'] = $signa->populate();
		$this->load->helper('number2word');
		$this->load->view('payee_checks_preview', $this->data );
	}
	
	public function save( $id ) {
		if( count($this->input->post()) > 0) {
			$this->form_validation->set_rules('check_num', 'Check Number', 'required');
			$this->form_validation->set_rules('date', 'Date', 'required');
			$this->form_validation->set_rules('amount', 'Amount', 'required');
			//$this->form_validation->set_rules('signatory[]', 'Signatory', 'required');
			if( $this->form_validation->run() == TRUE ) {
				$this->load->model('Checks_model');
				$checks = new $this->Checks_model;
				$checks->setCheckNum( $this->input->post("check_num") );
				$checks->setPayee( $id );
				$checks->setDate( $this->input->post("date") );
				$checks->setAmount( $this->input->post("amount") );
				$checks->setSignatory( json_encode( $this->input->post("signatory") ) );
				if( $checks->insert() ) {
					header("location: " . site_url("payee/checks/" .$id . "?print=" . $checks->getId()));
					exit;
				}
				print_r( $this->input->post() );
			} else {
				redirect("payee", "location");
			}			
		} else {
			redirect("payee", "location");
		}
	}
}
