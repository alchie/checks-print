<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checks extends CI_Controller {

	var $data = array();
	
	public function __construct() {
		parent::__construct();
		$this->load->model('Payee_model');
		$this->load->model('Signatory_model');
		$this->load->model('Checks_model');
	}
	
	public function index()
	{
		redirect("payee", "location");
	}
	
	public function preview( $id ) {
		
		$check = new $this->Checks_model;
		$check->setId($id, true);
		$check_data = $check->get();
		$this->data['check_data'] = $check_data;
		
		$payee = new $this->Payee_model;
		$payee->setId($check_data->payee, true);
		$this->data['payee'] = $payee->get();
		
		$signa = new $this->Signatory_model;
		$signa->setId(json_decode($check_data->signatory), true, false, NULL, 'where_in');
		$signa->setOrder('priority', 'ASC');
		$this->data['signatories'] = $signa->populate();
		
		$this->load->helper('number2word');
		$this->load->view('checks_preview', $this->data );
	}

	public function edit( $id ) {
		
		$check = new $this->Checks_model;
		$check->setId($id, true);
		
		if( count($this->input->post()) > 0) {
			$this->form_validation->set_rules('check_num', 'Check Number', 'required');
			$this->form_validation->set_rules('date', 'Date', 'required');
			$this->form_validation->set_rules('amount', 'Amount', 'required');
			if( $this->form_validation->run() == TRUE ) {
				$check->setCheckNum( $this->input->post("check_num"), false, true );
				$check->setDate( $this->input->post("date") , false, true);
				$check->setAmount( $this->input->post("amount"), false, true );
				$check->setSignatory( json_encode( $this->input->post("signatory") ) , false, true );
				if( $check->update() ) {
					header("location: " . site_url("checks/preview/" .$id) );
					exit;
				}
			} 		
		} 
		$check_data = $check->get();
		$this->data['check_data'] = $check_data;

		$payee = new $this->Payee_model;
		$payee->setId($check_data->payee, true);
		$this->data['payee'] = $payee->get();
		
		$this->load->model('Signatory_model');
		$signa = new $this->Signatory_model;
		$this->data['signatories'] = $signa->populate();

		$this->load->view('checks_edit', $this->data );
	}
	
	public function pr1nt( $id ) {
		$check = new $this->Checks_model;
		$check->setId($id, true);
		$check_data = $check->get();
		$this->data['check_data'] = $check_data;
		
		$payee = new $this->Payee_model;
		$payee->setId($check_data->payee, true);
		$this->data['payee'] = $payee->get();
		
		$signa = new $this->Signatory_model;
		$signa->setId(json_decode($check_data->signatory), true, false, NULL, 'where_in');
		$signa->setOrder('priority', 'ASC');
		$this->data['signatories'] = $signa->populate();
		
		$this->load->model("Settings_model");
		$settings = new $this->Settings_model;
		$svs = array();
		$settings_val = $settings->populate();
		foreach( $settings_val as $sv ) {
			$svs[$sv->key] = $sv->value;
		}
		$this->data['svs'] = $svs;
		
		$this->load->helper('number2word');
		$this->load->view('checks_print', $this->data );
	}
}
