<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller {

	var $data = array();
	
	public function __construct() {
		parent::__construct();
		$this->load->model('Signatory_model');
	}
		
	public function index() {
		$this->load->model("Settings_model");
		$settings = new $this->Settings_model;
		
		if( count($this->input->post()) > 0) {
			
			foreach($this->input->post("settings") as $key => $val ) {
				$settings->setKey( $key, TRUE );
				$settings->setValue( $val );
				if( $settings->nonEmpty() === TRUE ) {
					$settings->update();
				} else {
					$settings->insert();
				}
			}
		}
		
		$svs = array();
		$settings_val = $settings->populate();
		foreach( $settings_val as $sv ) {
			$svs[$sv->key] = $sv->value;
		}
		$this->data['svs'] = $svs;
		$this->load->view('settings', $this->data );
	}
	
}
