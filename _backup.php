<?php

define('BASEPATH', '');
define('ENVIRONMENT', 'production');
require_once("_core/application/config/database.php");
$dbconn = $db[$active_group];

function gzCompressFile($source, $level = 9){ 
    $dest = $source . '.gz'; 
    $mode = 'wb' . $level; 
    $error = false; 
    if ($fp_out = gzopen($dest, $mode)) { 
        if ($fp_in = fopen($source,'rb')) { 
            while (!feof($fp_in)) 
                gzwrite($fp_out, fread($fp_in, 1024 * 512)); 
            fclose($fp_in); 
        } else {
            $error = true; 
        }
        gzclose($fp_out); 
    } else {
        $error = true; 
    }
    if ($error)
        return false; 
    else
        return $dest; 
} 

function backup_tables($dbconn)
{

    $link = mysqli_connect($dbconn['hostname'],$dbconn['username'],$dbconn['password']);
    mysqli_select_db($link, $dbconn['database']);
    mysqli_query($link,"SET NAMES 'utf8'");


	$tables = array();
	$result = mysqli_query($link,'SHOW TABLES');
	while($row = mysqli_fetch_row($result))
	{
		$tables[] = $row[0];
	}

    $return='';
    //cycle through
    foreach($tables as $table)
    {
        $result = mysqli_query($link,'SELECT * FROM '.$table);
        $num_fields = mysqli_num_fields($result);

        $return.= 'DROP TABLE '.$table.';';
        $row2 = mysqli_fetch_row(mysqli_query($link,'SHOW CREATE TABLE '.$table));
        $return.= "\n\n".$row2[1].";\n\n";

        for ($i = 0; $i < $num_fields; $i++) 
        {
            while($row = mysqli_fetch_row($result))
            {
                $return.= 'INSERT INTO '.$table.' VALUES(';
                for($j=0; $j<$num_fields; $j++) 
                {
                    $row[$j] = addslashes($row[$j]);
                    $row[$j] = str_replace("\n","\\n",$row[$j]);
                    if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
                    if ($j<($num_fields-1)) { $return.= ','; }
                }
                $return.= ");\n";
            }
        }
        $return.="\n\n\n";
    }

    //save file
	$dir = "D:\QB Backup\checks_print_backup";
	$filename = $dir . '\db-backup-'.time().'-'.(md5(implode(',',$tables))).'.sql';
    $handle = fopen($filename,'w+');
    fwrite($handle,$return);
    fclose($handle);
	return $filename;
}

$filename = backup_tables($dbconn);
gzCompressFile( $filename );
unlink($filename);
?>